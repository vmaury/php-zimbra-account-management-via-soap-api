<?php

/*
 * Copyright (C) 20xx VMA Vincent Maury <vmaury@timgroup.fr>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY
 * 
 * Class to deal with zimbra Server with Soap API : create Account, check Account, lock account
 * 
 */

require_once './soap_envelopes.php';

class ServiceZimbra {

	/**
	 * 
	 * @var filename cookie file name
	 */
	private $cookFileName;

	/**
	 * 
	 * @var string zimbra token
	 */
	public $token;

	/**
	 * 
	 * @var handle curl 
	 */
	private $crl;

	/**
	 * 
	 * @var bool use Cookie
	 */
	private $useCookie = true;

	/**
	 * 
	 * @var type
	 */
	private $xmlDebug = false;
	private $zimbra_apiurl;
	private $zimbra_apiurl_user;
	private $zimbra_apiurl_pwd;

	/**
	 * @param type $zimbra_apiurl
	 * @param type $zimbra_apiurl_user
	 * @param type $zimbra_apiurl_pwd
	 */
	function __construct($zimbra_apiurl, $zimbra_apiurl_user, $zimbra_apiurl_pwd) {
		$this->zimbra_apiurl = $zimbra_apiurl;
		$this->zimbra_apiurl_user = $zimbra_apiurl_user;
		$this->zimbra_apiurl_pwd = $zimbra_apiurl_pwd;
		$this->cookFileName = tempnam(sys_get_temp_dir(), 'zimCook');
		//$this->connectAndAuth();
	}

	function test() {
		$rs = $this->connectAndAuth();
		if (!$rs->error) {
			if (!$rs->info)
				$rs->info = 'Ok';
		}
		return $rs;
	}

	/** create Account zimbra
	 * 
	 * @param type $name
	 * @param type $givenName
	 * @param type $cn
	 * @param type $sn
	 * @param type $displayName
	 */
	public function createAccount($name, $givenName = '', $cn = '', $sn = '', $displayName = '') {
		$rs = new \stdClass();
		$rs->error = '';
		$xmlCA = getXmlCreateAccount($this->token, $name, $givenName, $cn, $sn, $displayName);
		if ($this->xmlDebug)
			$rs->xmlSent = $xmlCA;

		$zimret = $this->zimCurlSendData($xmlCA, false);
		if ($zimret->res == 1 && !(empty($zimret->result))) {
			$zimex = $this->extractFromZimXml($zimret->result, 'CreateAccountResponse');
			if ($this->xmlDebug)
				$rs->resultbrut = $zimex;
			if (!$zimex->error) {
				$rs->result = true;
				$rs->info['mail'] = $name;
				foreach ($zimex->result->account->a as $at) {
					if (strval($at->attributes()[0]) == 'uid')
						$rs->info['uid'] = strval($at);
//					print_r($at->attributes());
					//$rs->info[strval($at->attributes()['n'])] = strval($at);
				}
			} else {
				$rs->error = $zimex->errorCode;
				$rs->info = $zimex->errorDetail;
			}
		} else {
			$rs->error = 'Serveur inaccessible ou résultat vide';
		}
		return $rs;
	}

	/** modif Account zimbra
	 * 
	 * @param string $name
	 * @param array $attrs key->val ex. ['zimbraAccountStatus' => 'active', 'cn'=>'dupont']
	 * 
	 * rem : zimbraAccountStatus must be one of: active,maintenance,locked,closed,lockout,pending
	 */
	public function modifyAccount($name, $attrs) {
		$rs = new \stdClass();
		$rs->error = '';

		$accountInfo = $this->getAccountInfo($name);
		if (!$accountInfo->error && !empty($accountInfo->info['zimbraId'])) { // on est obligé de récup l'id, ça marche pas avec le name
			$xmlMA = getXmlModifyAccount($this->token, $accountInfo->info['zimbraId'], $attrs);
			if ($this->xmlDebug)
				$rs->xmlSent = $xmlMA;

			$zimret = $this->zimCurlSendData($xmlMA, false);
			if ($zimret->res == 1 && !(empty($zimret->result))) {
				$zimex = $this->extractFromZimXml($zimret->result, 'ModifyAccountResponse');
				if ($this->xmlDebug)
					$rs->resultbrut = $zimex;
				if (!$zimex->error) {
					$rs->result = true;
					$rs->info['mail'] = $name;
					foreach ($zimex->result->account->a as $at) {
						$rs->info[strval($at->attributes()['n'])] = strval($at);
					}
				} else {
					$rs->error = $zimex->errorCode;
					$rs->info = $zimex->errorDetail;
				}
			} else {
				$rs->error = 'Serveur inaccessible ou résultat vide';
			}
		} else {
			$rs->error = $accountInfo->error;
			$rs->info = $accountInfo->info;
		}



		return $rs;
	}

	/** créer un compte mail zimbra
	 *
	 * @param type $name
	 * @param array $perso $perso['givenName'], $perso['cn'], $perso['sn'], $perso['cn']
	 * @param type $alias deprecated
	 * @return type
	 */
	function creerZimCompte($name, $perso, $alias = '') {
		//$cmd = chemCmd . "ca '" . $mail . "' '' cn '" . $perso['cn'] .
		//    "' displayName '" . $perso['cn'] . "' givenName '" . $perso['givenName'] . "' sn '" . $perso['sn'] . "'";
		return $this->createAccount($name, $perso['givenName'], $perso['cn'], $perso['sn'], $perso['cn']);
		if ($alias) {
			$cmd .= ";" . chemCmd . "aaa " . $mail . " " . $alias;
			// on fait rien flemme
			// voir si besoin https://files.zimbra.com/docs/soap_api/8.0/soapapi-zimbra-doc/api-reference/zimbraAdmin/AddAccountAlias.html
		}
	}

	/** désactive un compte mail zimbra
	 *
	 * @param type $name
	 * @return type
	 */
	function lockZimCompte($name) {
		return $this->modifyAccount($name, ['zimbraAccountStatus' => 'locked']);
		//$cmd = chemCmd . "ma '" . $mail . "' zimbraAccountStatus locked";
	}

	/** active un compte mail zimbra
	 *
	 * @param type $name
	 * @return type
	 */
	function activeZimCompte($name) {
		return $this->modifyAccount($name, ['zimbraAccountStatus' => 'active']);
		//$cmd = chemCmd . "ma '" . $mail . "' zimbraAccountStatus active";
	}

	/** Account Info, mais sans les attributs (ne sert pas à grand'chose, voir getCompteInfos)
	 * 
	 * @param type $name
	 * @return type
	 */
	public function getAccountInfo($name) {
		$rs = new \stdClass();
		$rs->error = '';

		$xmlAI = getXmlAccountInfo($this->token, $name);
		//$rs->xmlSend = $xmlAI;
		//echo "Xml Account Info ".$xmlAI."\n";
		$zimret = $this->zimCurlSendData($xmlAI, false);
		if ($zimret->res == 1 && !(empty($zimret->result))) {
			$zimex = $this->extractFromZimXml($zimret->result, 'GetAccountInfoResponse');
			if ($this->xmlDebug)
				$rs->resultbrut = $zimex;
			if (!$zimex->error) {
				foreach ($zimex->result->a as $at) {
					//print_r($at->attributes());
					$rs->info[strval($at->attributes()['n'])] = strval($at);
				}
			} else {
				$rs->error = $zimex->errorCode;
				$rs->info = $zimex->errorDetail;
			}
		} else {
			$rs->error = 'Serveur inaccessible ou résultat vide';
		}
		return $rs;
	}

	/** renvoie les attributs d'un compte
	 *
	 * @param string $name
	 * @param string $attrs : liste des attributs à renvoyer séparés par des virg
	 * @return type
	 */
	public function getCompteInfos($name, $attrs = "cn,displayName,givenName,mail,sn,uid,zimbraAccountStatus") {
		$rs = new \stdClass();
		$rs->error = '';

		$xmlAI = getXmlAccount($this->token, $name, $attrs);
		if ($this->xmlDebug)
			$rs->xmlSent = $xmlAI;
		//echo "Xml Account Info ".$xmlAI."\n";
		$zimret = $this->zimCurlSendData($xmlAI, false);
		if ($zimret->res == 1 && !(empty($zimret->result))) {
			$zimex = $this->extractFromZimXml($zimret->result, 'GetAccountResponse');
			if ($this->xmlDebug)
				$rs->resultbrut = $zimex;
			if (!$zimex->error) {
				foreach ($zimex->result->account->a as $at) {
					//print_r($at->attributes());
					$rs->info[strval($at->attributes()['n'])] = strval($at);
				}
			} else {
				$rs->error = $zimex->errorCode;
				$rs->info = $zimex->errorDetail;
			}
		} else {
			$rs->error = 'Serveur inaccessible ou résultat vide';
		}
		return $rs;
	}

	public function connectAndAuth() {
		$rs = new \stdClass();
		$rs->error = $rs->info = '';

		$authEnv = getXmlAuthEnv($this->zimbra_apiurl_user, $this->zimbra_apiurl_pwd);
		if ($this->xmlDebug) $rs->xmlSent = $authEnv;
		$this->crl = curl_init($this->zimbra_apiurl);
		/** Auth * */
		$zimret = $this->zimCurlSendData($authEnv, false);

		// print_r($zimret);
		if ($zimret->res == 1) {
			if ($zimret->http_code == 500) { // parfois ça marche pas avec les cookies ?? 
				$this->useCookie = false;
				$zimret = $this->zimCurlSendData($authEnv, true);
				if ($zimret->token) {
					$this->token = $zimret->token;
					$rs->info = 'Token récupéré sans cookie';
				} else
					$rs->error = "Impossible de récupérer le token d'auth";
			} else {
				$zrObj = $this->extractFromZimXml($zimret->result, 'AuthResponse');
				if (!$zrObj->error) {
					$this->token = $zrObj->result->authToken;
					$rs->info = 'Auth ok, avec cookie';
				} else {
					return $zrObj;
				}
			}
		} else {
			$rs->error = "Connexion serveur impossible";
		}
		return $rs;
	}

	/** Send data to Zimbra 
	 * 
	 * @param type $data data to post
	 * @param bool $opt_header if we want to retrieve header of response
	 * @return \stdClass
	 */
	private function zimCurlSendData($data, $opt_header = false) {
		$ret = new \stdClass();
		curl_setopt($this->crl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($this->crl, CURLINFO_HEADER_OUT, true	);
		curl_setopt($this->crl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($this->crl, CURLOPT_HEADER, $opt_header); // c'est ça qui fait que ça crache le header
		curl_setopt($this->crl, CURLOPT_POST, true);
		curl_setopt($this->crl, CURLOPT_POSTFIELDS, $data);
		//curl_setopt($this->crl, CURLOPT_, $authEnv);
		curl_setopt($this->crl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($this->crl, CURLOPT_SSL_VERIFYPEER, 0);
		if ($this->useCookie) {
			$cookieFile = $this->cookFileName;
			//$cookie = 'cookie.txt';
			//echo "cookie file $cookieFile\n";
			curl_setopt($this->crl, CURLOPT_COOKIEFILE, $cookieFile);
			curl_setopt($this->crl, CURLOPT_COOKIEJAR, $cookieFile);
		}
		// Set HTTP Header for POST request 
		curl_setopt($this->crl, CURLOPT_HTTPHEADER, array(
			//'Content-Type: text/xml;charset="utf-8"',
			'Content-Type: application/soap+xml',
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"Content-length: " . strlen($data)
		));

		// Submit the POST request
		$result = curl_exec($this->crl);

		// handle curl error
		if ($result === false) {
			// throw new Exception('Curl error: ' . curl_error($this->crl));
			//print_r('Curl error: ' . curl_error($this->crl));
			$ret->res = false;
			$ret->error = 'Curl error: ' . curl_error($this->crl);
		} else {
			$ret->res = true;
			$ret->http_code = curl_getinfo($this->crl, CURLINFO_HTTP_CODE);
			$cookies = array();
			preg_match_all('/Set-Cookie:(?<cookie>\s{0,}.*)$/im', $result, $cookies);

			//echo "the cookie = ";print_r($cookies['cookie']); // show harvested cookies
			/* Array	(
			  [0] =>  ZM_ADMIN_AUTH_TOKEN=0_4e8215dcfb5ed0dd85f07a5e4182a3ffe4640d1b_69643d33363a31383232326336642d366364642d346135362d623037622d6634346434303233363939313b6578703d31333a313635383436373835333036343b61646d696e3d313a313b747970653d363a7a696d6272613b753d313a613b7469643d393a3435333637303534363b76657273696f6e3d31343a382e382e31355f47415f333832393b;Path=/;Secure;HttpOnly
			  ) */
			if (isset($cookies['cookie'][0])) {
				list($tcook, $token) = explode('=', $cookies['cookie'][0]);
				if (trim($tcook) == 'ZM_ADMIN_AUTH_TOKEN') {
					list($token) = explode(';', $token);
					$ret->token = $token;
				} else
					$ret->error = "bad coookie type $tcook\n";
			} //else echo "bad coookie type $tcook\n";
		}
		$ret->result = $result;
		//print_r($result);
		return $ret;
	}

	/** Extract an usefull object in an zimbra xml response
	 * I've tried to keep this as KISS as possible (I spent a lot of time on this shit :-\)
	 * 
	 * @param string $xmlbrut
	 * @param string $node
	 * @return stdClass Object ->error or ->result=SimpleXMLElement Object|array of SimpleXMLElement Object
	 */
	function extractFromZimXml($xmlbrut, $node) {
		$xml = new \SimpleXMLElement($xmlbrut);
		$ret = new \stdClass();
		if ($this->xmlDebug)
			$ret->xmlbrut = $xmlbrut;
		$xml->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope');

		$rerr = $xml->xpath("//soap:Body/soap:Fault"); // first we're looking for errors
		if (is_array($rerr) && isset($rerr[0]) && is_object($rerr[0])) {
			$ret->error = 1;
			$ret->errorDetail = strval($rerr[0]->faultstring);
			$ret->errorCode = strval($rerr[0]->detail->Error->Code);
			return $ret;
		} else { // it seems to be ok, the we parse what's interesting
			$xml->registerXPathNamespace('zimbra', 'urn:zimbraAdmin');
			$xret = $xml->xpath("//soap:Body/zimbra:$node");
			if (is_array($xret)) {
				$ret->error = 0;
				if (count($xret) == 1) {
					$ret->result = $xret[0];
				} else
					$ret->result = $xret;
			} else {
				$ret->error = 1;
				$ret->errorDetail = "Empty result : maybe $node does not exist";
				$ret->errorCode = "EmptyResult";
			}
			return $ret;
		}

		/* Exembple de retour pour GetAccountInfo :
		  Array (
		  [0] => SimpleXMLElement Object
		  (
		  [name] => test@zzz.fr
		  [a] => Array
		  (
		  [0] => 3f42aebe-3c45-4a9a-8201-a1ead8f3e95a
		  [1] => xxx.zzz.lan
		  )

		  [cos] => SimpleXMLElement Object
		  (
		  [@attributes] => Array
		  (
		  [name] => default
		  [id] => e00428a1-0c00-11d9-836a-000d93afea2a
		  )

		  )

		  [soapURL] => https://xxx.zzz.lan:8443/service/soap/
		  [adminSoapURL] => https://xxx.zzz.lan:7071/service/admin/soap/
		  [publicMailURL] => https://xxx.zzz.lan:8443
		  )
		  ) */
	}

}
