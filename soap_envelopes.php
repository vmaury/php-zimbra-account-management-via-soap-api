<?php

/*
 * Copyright (C) 20xx VMA Vincent Maury <vmaury@timgroup.fr>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY
 */

/** recup XML à envoyer pr auth
 * 
 * @param type $login
 * @param type $password
 * @return XML
 */
function getXmlAuthEnv($login, $password) {
	return <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	<soapenv:Header>
	  <urn:context xmlns:urn="urn:zimbraAdmin"/>
   </soapenv:Header>
   <soapenv:Body>
	  <urn1:AuthRequest xmlns:urn1="urn:zimbraAdmin">
		 <urn1:account by="name">$login</urn1:account>
		 <urn1:password>$password</urn1:password>
	  </urn1:AuthRequest>
   </soapenv:Body>
</soapenv:Envelope>
XML;
}

/** recup XML à envoyer pr account info
 * 
 * @param type $token
 * @param type $name
 * @return XML
 */
function getXmlAccountInfo($token, $name) {
	return <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header>
      <context xmlns="urn:zimbraAdmin">
		<authToken>$token</authToken>
      </context>
   </soapenv:Header>
   <soapenv:Body>
		<GetAccountInfoRequest xmlns="urn:zimbraAdmin">
			<account by="name">$name</account>
		</GetAccountInfoRequest>	
   </soapenv:Body>
</soapenv:Envelope>
XML;
}

/** recup XML à envoyer pr account info
 * 
 * @param string $token
 * @param string $name
 * @param string $attrs liste des attributs séparés par des virg
 * @return XML
 */
function getXmlAccount($token, $name, $attrs = "cn,displayName,givenName,mail,sn,uid,zimbraAccountStatus") {
	if (!empty($attrs)) $attrs = ' attrs="' . $attrs . '" ';
	return <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header>
      <context xmlns="urn:zimbraAdmin">
		<authToken>$token</authToken>
      </context>
   </soapenv:Header>
   <soapenv:Body>
		<GetAccountRequest $attrs xmlns="urn:zimbraAdmin">
			<account by="name">$name</account>
		</GetAccountRequest>	
   </soapenv:Body>
</soapenv:Envelope>
XML;
}

/** recup XML pr creer account
 * 
 * @param type $token
 * @param type $name
 * @param type $givenName
 * @param type $cn
 * @param type $sn
 * @param type $displayName
 * @return type
 */
function getXmlCreateAccount($token, $name, $givenName = '', $cn = '', $sn = '', $displayName = '') {
	if ($givenName == '') $givenName = $name;
	if ($cn == '') $cn = $name;
	if ($sn == '') $sn = $name;
	if ($displayName == '')	$displayName = $cn;

	return <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header>
      <context xmlns="urn:zimbraAdmin">
		<authToken>$token</authToken>
      </context>
   </soapenv:Header>
   <soapenv:Body>
		<CreateAccountRequest xmlns="urn:zimbraAdmin" name="$name">
			<a n="givenName">$givenName</a>
			<a n="sn">$sn</a>
			<a n="cn">$cn</a>
			<a n="displayName">$displayName</a>
		</CreateAccountRequest>	
   </soapenv:Body>
</soapenv:Envelope>
XML;
}

/** recup XML pr creer account
 * 
 * @param type $token
 * @param type $id ya que ça qui marche !
 * @param array $attrs
 * @return type
 */
function getXmlModifyAccount($token, $id, $attrs) {
	$strAttr = '';
	foreach ($attrs as $key => $val) {
		$strAttr .= '<a n="' . $key . '">' . $val . "</a>\n";
	}

	return <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header>
      <context xmlns="urn:zimbraAdmin">
		<authToken>$token</authToken>
      </context>
   </soapenv:Header>
   <soapenv:Body>
		<ModifyAccountRequest xmlns="urn:zimbraAdmin" id="$id">
			$strAttr
		</ModifyAccountRequest>	
   </soapenv:Body>
</soapenv:Envelope>
XML;
}
