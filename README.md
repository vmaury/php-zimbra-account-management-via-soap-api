# ZimbraPoc : POC to manage accounts via Zimbra Soap API Services
-----------------------------------------------------------------

This script is designed for managing zimbra accounts via Zimbra Soap API Services

(and replacing zmprov commands who have to be run via ssh)

The zimbra doc can be found here https://files.zimbra.com/docs/soap_api/8.0/soapapi-zimbra-doc/api-reference/overview-summary.html
The services to mange accounts are not in zimbraAccount but in zimbraAdmin
This doc is not very "clear" :-\

This script is supposed to be as stupid simple as possible (KISS)
It doesnt need Guzzle, only php-curl.
You need to copy config.inc.dist.php to config.inc.php and modify it with your infos
it needs only 3/4 files
 - index.php : examples of service calls
 - config.inc.php : the conf
 - ServiceZimbra.php : class with methods to call the API
 - soap_envelopes.php : XML Soap enveloppes models to be sent to zimbra

Connecting to zimbra is quite tricky :
 - the more simple is to accept cookies with curl, but it's not very reliable
 - a token can be used, but to retrieve it the first curl call retrive a 500 HTTP error, but the token can be extracted from the header
 - I'm not an xml master, but/so extracting responses AND errors (faults) from the zimbra xml return took me some effort  


ServiceZimbra.php can be used as a symfony service


